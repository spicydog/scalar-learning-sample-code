// 2020-10-10

object PrimeNumbers {

	def isPrime(n: Int) : Boolean = {
		if (n <= 1)
			return false

		val divisibles = (2 to (n-1)).filter(elem => 
			n % elem == 0
		)

		divisibles.length == 0
	}

    def main(args: Array[String]) = {

    	val n = 1000
    	val primeNumbers = (1 to n).filter(elem =>
    		isPrime(elem)
    	)

    	print(primeNumbers)
    }
}